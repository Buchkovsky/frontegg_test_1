image-name="listat88/frontegg_test_1"

build:
	docker build -t $(image-name) .

run:
	docker run -p 80:80 -d $(image-name)

tag:
	docker tag $(image-name) $(image-name):latest

push:
	docker push $(image-name)